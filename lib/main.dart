//analiza interfaców

import 'package:flutter/material.dart';

const Color darkBlue = Color.fromARGB(255, 18, 32, 47);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
//       theme: ThemeData.dark().copyWith(
//         scaffoldBackgroundColor: darkBlue,
//       ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: MainView(),
        ),
      ),
    );
  }
}

class MainView extends StatefulWidget {
  const MainView({super.key});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  @override
  Widget build(BuildContext context) {
    const double width = 1170 * 0.35;
    const double height = 2232 * 0.35;

    return Scaffold(
      body: Container(
        color: Colors.grey[300],
        child: Center(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const SizedBox.shrink(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        width: width,
                        height: height,
                        color: Colors.green,
                        child: TestView(width: width, height: height)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        width: width,
                        height: height,
                        color: Colors.green,
                        child: TestView(width: width, height: height)),
                  ),
                  const SizedBox.shrink(),
                ],
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceAround,
              //   children: [
              //     const SizedBox.shrink(),
              //     FilledButton(onPressed: () {}, child: Text('wybierz')),
              //     FilledButton(onPressed: () {}, child: Text('wybierz')),
              //     const SizedBox.shrink(),
              //   ],
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

class TestView extends StatefulWidget {
  const TestView({super.key, required this.width, required this.height});
  final width;
  final height;
  @override
  State<TestView> createState() => _TestViewState();
}

class _TestViewState extends State<TestView> {
  ///-----------------------
  MaterialColor myColor = Colors.red;
  Position appBarPosition = Position.bot;
  Position fabPosition = Position.right;
  bool appBar = true;

  ///----------------------

  @override
  Widget build(BuildContext context) {
    // final width = MediaQuery.of(context).size.width;
    final double cardHeight = widget.height * 0.09;
    final double iconSize = widget.height * 0.03;
    final double appBarHeight = widget.height * 0.08;
    final double cardWidth = widget.width * 0.5;
    final myStyle = TextStyle(
      fontSize: widget.height *
          0.08 *
          0.22, // Dostosuj rozmiar czcionki w elemencie listy
    );
    final myStyleSub = TextStyle(
      fontSize: widget.height *
          0.08 *
          0.16, // Dostosuj rozmiar czcionki w elemencie listy
    );
    return Scaffold(
      appBar: appBar
          ? AppBar(
              backgroundColor: Colors.white, // Białe tło AppBar
              title: const Center(
                child: Text(
                  'Aplikacja do wszystkiego',
                  style: TextStyle(
                    color: Colors.black, // Czarna czcionka
                  ),
                ),
              ),
            )
          : null,
      backgroundColor: myColor[200],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            appBarPosition == Position.top
                ? MyBar(myColor: myColor, appBarHeight: appBarHeight)
                : SizedBox(height: appBarHeight),
            appBarPosition == Position.top
                ? Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: MyFab(
                        myColor: myColor,
                        position: fabPosition,
                        appBarHeight: appBarHeight),
                  )
                : SizedBox(height: appBarHeight),
            Wrap(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.red;
                          appBarPosition = Position.top;
                          fabPosition = Position.left;
                        });
                      },
                      title: Text("Lorem ipsum dolor sit amet", style: myStyle),
                      subtitle: Text("czerwony, góra, lewo", style: myStyleSub),
                      leading: Icon(
                        Icons.favorite,
                        color: myColor,
                        size: iconSize,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.blue;
                          appBarPosition = Position.top;
                          fabPosition = Position.left;
                        });
                      },
                      title: Text("Ut enim ad minim veniam", style: myStyle),
                      subtitle:
                          Text("niebieski, góra, lewo", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.red;
                          appBarPosition = Position.top;
                          fabPosition = Position.center;
                        });
                      },
                      title: Text("Vero eos et accusamus", style: myStyle),
                      subtitle:
                          Text("czerwony, góra, środek", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.blue;
                          appBarPosition = Position.top;
                          fabPosition = Position.center;
                        });
                      },
                      title: Text("Qui officia deserunt ", style: myStyle),
                      subtitle:
                          Text("niebieski, góra, środek", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.red;
                          appBarPosition = Position.top;
                          fabPosition = Position.right;
                        });
                      },
                      title: Text("Rerum hic tenetur", style: myStyle),
                      subtitle:
                          Text("czerwony, góra, prawo", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.blue;
                          appBarPosition = Position.top;
                          fabPosition = Position.right;
                        });
                      },
                      title: Text("Excepteur sint occaecat", style: myStyle),
                      subtitle:
                          Text("niebieski, góra, prawo", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.red;
                          appBarPosition = Position.bot;
                          fabPosition = Position.left;
                        });
                      },
                      title: Text("Lorem ipsum dolor sit amet", style: myStyle),
                      subtitle: Text("czerwony, dół, lewo", style: myStyleSub),
                      leading: Icon(
                        Icons.favorite,
                        color: myColor,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.blue;
                          appBarPosition = Position.bot;
                          fabPosition = Position.left;
                        });
                      },
                      title: Text("Ut enim ad minim veniam", style: myStyle),
                      subtitle: Text("niebieski, dół, lewo", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.red;
                          appBarPosition = Position.bot;
                          fabPosition = Position.center;
                        });
                      },
                      title: Text("Vero eos et accusamus", style: myStyle),
                      subtitle:
                          Text("czerwony, dół, środek", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.blue;
                          appBarPosition = Position.bot;
                          fabPosition = Position.center;
                        });
                      },
                      title: Text("Qui officia deserunt", style: myStyle),
                      subtitle:
                          Text("niebieski, dół, środek", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.red;
                          appBarPosition = Position.bot;
                          fabPosition = Position.right;
                        });
                      },
                      title: Text("Rerum hic tenetur", style: myStyle),
                      subtitle: Text("czerwony, dół, prawo", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
                SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Card(
                    child: ListTile(
                      onTap: () {
                        setState(() {
                          myColor = Colors.blue;
                          appBarPosition = Position.bot;
                          fabPosition = Position.right;
                        });
                      },
                      title: Text("Excepteur sint occaecat", style: myStyle),
                      subtitle:
                          Text("niebieski, dół, prawo", style: myStyleSub),
                      leading: Icon(Icons.favorite, color: myColor),
                    ),
                  ),
                ),
              ],
            ),
            appBarPosition == Position.bot
                ? Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: MyFab(
                        myColor: myColor,
                        position: fabPosition,
                        appBarHeight: appBarHeight),
                  )
                : SizedBox(height: appBarHeight),
            appBarPosition == Position.bot
                ? MyBar(myColor: myColor, appBarHeight: appBarHeight)
                : SizedBox(height: appBarHeight),
          ],
        ),
      ),
    );
  }
}

enum Position { top, bot, left, center, right }

class MyBar extends StatelessWidget {
  final myColor;
  final double appBarHeight;
  const MyBar({super.key, required this.myColor, required this.appBarHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: NavigationBar(
        height: appBarHeight,
        backgroundColor: myColor,
        destinations: [
          TextButton.icon(
            onPressed: () {},
            style:
                ButtonStyle(backgroundColor: MaterialStatePropertyAll(myColor)),
            icon: const Icon(
              Icons.search,
              color: Colors.white,
            ),
            label: const Text(
              'Search',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          TextButton.icon(
            onPressed: () {},
            style:
                ButtonStyle(backgroundColor: MaterialStatePropertyAll(myColor)),
            icon: const Icon(
              Icons.menu,
              color: Colors.white,
            ),
            label: const Text(
              'Menu',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          TextButton.icon(
            onPressed: () {},
            style:
                ButtonStyle(backgroundColor: MaterialStatePropertyAll(myColor)),
            icon: const Icon(
              Icons.person,
              color: Colors.white,
            ),
            label: const Text(
              'Profile',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          TextButton.icon(
            onPressed: () {},
            style:
                ButtonStyle(backgroundColor: MaterialStatePropertyAll(myColor)),
            icon: const Icon(
              Icons.settings,
              color: Colors.white,
            ),
            label: const Text(
              'Options',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyFab extends StatelessWidget {
  const MyFab(
      {super.key,
      this.myColor,
      required this.appBarHeight,
      required this.position});
  final myColor;
  final double appBarHeight;
  final Position position;
  @override
  Widget build(BuildContext context) {
    switch (position) {
      case Position.left:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: appBarHeight,
              width: appBarHeight,
              child: FloatingActionButton(
                backgroundColor: myColor,
                onPressed: () {},
                child: const Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox.shrink(),
            const SizedBox.shrink(),
          ],
        );
      case Position.center:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox.shrink(),
            Container(
              height: appBarHeight,
              width: appBarHeight,
              child: FloatingActionButton(
                backgroundColor: myColor,
                onPressed: () {},
                child: const Icon(
                  Icons.add,
                ),
              ),
            ),
            const SizedBox.shrink(),
          ],
        );
      case Position.right:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox.shrink(),
            const SizedBox.shrink(),
            Container(
              height: appBarHeight,
              width: appBarHeight,
              child: FloatingActionButton(
                backgroundColor: myColor,
                onPressed: () {},
                child: const Icon(
                  Icons.add,
                ),
              ),
            ),
          ],
        );
      default:
        return const SizedBox.shrink();
    }
  }
}
